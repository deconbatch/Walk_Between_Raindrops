/**
 * Walk Between Raindrops.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created 2019.08.27
 * still images of Vector Field with parlin noise in angle.
 */

void setup() {

  size(980, 980);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();

}

void draw() {

  int   frmMax  = 3;
  float hueInit = random(360.0);

  translate(width * 0.5, height * 0.5);

  background(hueInit, 10.0, 90.0, 100);
  for (int frmCnt = 1; frmCnt <= frmMax; frmCnt++) {
    noiseSeed(floor(hueInit * frmCnt));
    drawBackground(hueInit + 30.0 * frmCnt);
  }

  for (int frmCnt = 1; frmCnt <= frmMax; frmCnt++) {
    noiseSeed(floor(hueInit * frmCnt));
    drawCells(hueInit + 30.0 * frmCnt);
    casing();
    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");
  }
  
  exit();

}

/**
 * drawBackground : draw background
 */
private void drawBackground(float _hueInit) {

  noFill();
  for (int i = 0; i < width; i++) {
    float side = map(noise(i*0.001), 0.0, 1.0, -height, height);
    strokeWeight(1.0);
    stroke(_hueInit % 360.0, 10.0, 90.0, 100.0);
    line(i - width * 0.5, side, i - width * 0.5, height * 0.5);
  }
  
}

/**
 * drawCells : draw cells
 */
private void drawCells(float _hueInit) {

  int cellCnt   = 10;
  int rippleCnt = floor(random(3.0, 10.0));
  
  int   ptnMax     = cellCnt * rippleCnt;
  float plotRadius = 0.25;
  float xPhase     = random(1.0);
  float yPhase     = random(1.0);

  for (int ptnCnt = 0; ptnCnt < ptnMax; ++ptnCnt) {
      
    int   plotMax   = floor(random(50.0, 100.0));
    float plotDiv   = random(0.05, 0.12);
    float noiseMult = random(2.0, 10.0);

    float ptnRatio = map(ptnCnt, 0, ptnMax, 0.0, 1.0);

    float rPoint = 0.0;
    float xPoint = plotRadius * cos(TWO_PI * (xPhase + ptnRatio) * rippleCnt);
    float yPoint = plotRadius * sin(TWO_PI * (yPhase + ptnRatio) * rippleCnt);

    //    float baseHue = _hueInit + map(ptnRatio * ptnRatio, 0.0, 1.0, 0.0, 120.0);
    float baseHue = _hueInit + (ptnCnt % cellCnt) * 10.0;
    float baseSat = map(ptnRatio, 0.0, 1.0, 20.0, 80.0);
    float baseBri = map(ptnRatio * ptnRatio, 0.0, 1.0, 80.0, 30.0);
    float baseAlp = map(ptnRatio, 0.0, 1.0, 10.0, 40.0);
    float baseWgt = (1.0 - sin(HALF_PI * ptnRatio)) * 150.0;

    strokeWeight(baseWgt * 0.5);
    stroke(baseHue % 360.0, baseSat, baseBri, baseAlp);
    noFill();

    beginShape();
    for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

      float plotRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);

      float rPrev = rPoint;
      float xPrev = xPoint;
      float yPrev = yPoint;

      float np = noise(xPrev * noiseMult, yPrev * noiseMult);
      rPoint += np;
      xPoint += plotDiv * cos(TWO_PI * rPoint);
      yPoint += plotDiv * sin(TWO_PI * rPoint);

      vertex(xPoint * width, yPoint * height);
      if (np > 0.2) {
        ellipse(xPoint * width, yPoint * height, baseWgt, baseWgt);
      }

    }
    endShape();
      
  }
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 60.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
